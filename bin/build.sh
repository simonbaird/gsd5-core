#!/bin/bash

GSD5_PATH=`pwd`

TIDDLYWIKI_PATH=$GSD5_PATH/../TiddlyWiki5
export TIDDLYWIKI_PLUGIN_PATH=$GSD5_PATH/plugins

OUTPUT_PATH=$GSD5_PATH/../gsd5-output
OUTPUT_FILE=gsd5-empty.html
if [ -d $OUTPUT_PATH ]; then
	mkdir -p $OUTPUT_PATH
fi

# Create build date versioning.
BUILD_DATE=`date -u +"%Y%m%d%H%M%S"`
sed -i --regexp-extended "s/(\"version\"\:\W\"[0-9]+\.[0-9]+\.[0-9]+\-*\w*)\+*[0-9]*\"/\1\+$BUILD_DATE\"/" $GSD5_PATH/plugins/gsd5/core/plugin.info

( node $TIDDLYWIKI_PATH/tiddlywiki.js \
	$GSD5_PATH/editions/gsd5 \
	--verbose \
	--output $OUTPUT_PATH \
	--build )

echo Wrote to $OUTPUT_PATH/$OUTPUT_FILE
